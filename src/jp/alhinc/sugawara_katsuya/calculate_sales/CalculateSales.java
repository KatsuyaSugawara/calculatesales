package jp.alhinc.sugawara_katsuya.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
		public static void main(String args[]){
			try{
				final String branchlst = "branch.lst";
				final String branchout = "branch.out";
				final String commoditylst = "commodity.lst";
				final String commodityout = "commodity.out";

				//支店定義ファイルを読み込み、Mapに格納する
				Map<String,String> branchlist = readBranchList(args[0],branchlst);

				//商品定義ファイルを読み込み、Mapに格納する
				Map<String,String> itemlist = readitemList(args[0],commoditylst);

				//売上ファイルを読み込み、支店別売上をMapに格納する
				Map<String,Long> branchsaleslist = readRcdList(args[0],branchlist).get(0);

				//売上ファイルを読み込み、商品別売上をMapに格納する
				Map<String,Long> itemsaleslist = readRcdList(args[0],branchlist).get(1);

				//支店別売上を金額順にファイルに書き込む
				writeOutFile(args[0],branchout,branchlist,branchsaleslist);

				//商品別売上を金額順にファイルに書き込む
				writeOutFile(args[0],commodityout,itemlist,itemsaleslist);

				}catch(FileNotFoundException e){
					System.out.println("支店定義ファイルが存在しません");
				return;
				}catch(NumberFormatException e){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}catch(IllegalArgumentException e){
					System.out.println(e);
					return;
				}catch(Exception e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
		}


		//支店定義ファイルを読み込むメソッド
		public static Map<String,String> readBranchList(String dir,String file) throws Exception{

			BufferedReader br = new BufferedReader(new FileReader(new File(dir,file)));

			Map<String,String> lstlist = new HashMap<>();
			String str;

				while((str =br.readLine()) != null){

					String[] s = str.split(",");

					if(s[0].length()!=3){
						br.close();
						throw new IllegalArgumentException("支店定義ファイルのフォーマットが不正です");
					}else{
						Long.parseLong(s[0]);
					}
					lstlist.put(s[0],s[1]);
				}
				br.close();
				return lstlist;
		}

		//商品定義ファイル読み込むメソッド
		public static Map<String,String> readitemList(String dir,String file) throws Exception{

			 BufferedReader br = new BufferedReader(new FileReader(new File(dir,file)));

			Map<String,String> lstlist = new HashMap<>();
			String str;

				while((str =br.readLine()) != null){

					String[] s = str.split(",");
					lstlist.put(s[0],s[1]);
				}
				br.close();
				return lstlist;
		}


		//売上ファイルを読み込み集計するメソッド
		public static ArrayList<Map<String,Long>> readRcdList(String dir,Map<String,String> branchlist) throws Exception{

			File f = null;
			BufferedReader br = null;
			ArrayList<Map<String,Long>> rcdlist = new ArrayList<>();
			ArrayList<String> branchrcdlist = new ArrayList<>();
			ArrayList<Long> salesrcdlist = new ArrayList<>();
			ArrayList<String> itemsrcdlist = new ArrayList<>();

			Map<String,Long> branchsalesmap= new HashMap<>();
			Map<String,Long> itemsalesmap = new HashMap<>();

			f = new File(dir);
			String rcd[] = f.list();

			int filenumber = 0;
			String linenumber = null;

			for(int i = 0;i<rcd.length;i++){
				if(rcd[i].endsWith("rcd") && (rcd[i].lastIndexOf("."))== 8){
					if(Integer.parseInt(rcd[i].substring(1,8)) != (filenumber+1)){

						throw new IllegalArgumentException("売上ファイル名が連番になっていません");

					}else{
						br = new BufferedReader(new FileReader(new File(dir,rcd[i])));

						branchrcdlist.add(br.readLine());
						itemsrcdlist.add(br.readLine());
						salesrcdlist.add(Long.parseLong(br.readLine()));

						linenumber = br.readLine();

						filenumber++;
					}
				}
				if(linenumber != null){
					throw new IllegalArgumentException(rcd[i] + "のフォーマットが不正です");
				}
			}

			for(int i = 0;i < branchrcdlist.size();i++){
				if(branchsalesmap.containsKey(branchrcdlist.get(i))){
					branchsalesmap.put(branchrcdlist.get(i), branchsalesmap.get(branchrcdlist.get(i))+salesrcdlist.get(i));
				}else{
				branchsalesmap.put(branchrcdlist.get(i), salesrcdlist.get(i));
				}
				if(salesrcdlist.get(i) >=1000000000){
					throw new IllegalArgumentException("合計金額が10桁を超えました");
				}else if(!(branchlist.containsKey(branchrcdlist.get(i)))){
					throw new IllegalArgumentException(rcd[i] + "の支店コードが不正です");
				}
			}

			for(int i = 0;i < itemsrcdlist.size();i++){
				if(itemsalesmap.containsKey(itemsrcdlist.get(i))){
					itemsalesmap.put(itemsrcdlist.get(i), itemsalesmap.get(itemsrcdlist.get(i))+salesrcdlist.get(i));
				}else{
					itemsalesmap.put(itemsrcdlist.get(i), salesrcdlist.get(i));
				}
				if(salesrcdlist.get(i) >=1000000000){
					throw new IllegalArgumentException("合計金額が10桁を超えました");
				}else if(!(branchlist.containsKey(branchrcdlist.get(i)))){
					throw new IllegalArgumentException(rcd[i] + "の支店コードが不正です");
				}
			}

			rcdlist.add(branchsalesmap);
			rcdlist.add(itemsalesmap);

			return rcdlist;
		}


		//金額順にソートして集計結果を出力するメソッド
		public static void writeOutFile(String dir,String file,Map<String,String> lstlist,Map<String,Long> rcdmap) throws Exception{

			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(dir,file)),"UTF8")));

			rcdmap.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).forEach(s -> pw.println(s.getKey() + "," + lstlist.get(s.getKey()) + "," + s.getValue()));
			pw.close();
		}
}
